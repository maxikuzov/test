<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<div class="sidebar-box search-form-wrap">
    <form action="#" class="search-form">
        <div class="form-group">
            <span class="icon fa fa-search"></span>
            <input type="text" class="form-control" id="s" placeholder="Type a keyword and hit enter">
        </div>
    </form>
</div>

<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("Тестовое задание");
?>

<?php
    $APPLICATION->IncludeComponent(
        "bitrix:main.feedback",
        "contacts",
        [
            "USE_CAPTCHA" => "N",
            "OK_TEXT" => "Форма успешно отправлена",
            "EMAIL_TO" => EMAIL_FEEDBACK_FORM,
            "REQUIRED_FIELDS" => [],
            "EVENT_MESSAGE_ID" => [
                FEEDBACK_FORM_MAIL_EVENT_ID
            ]
        ]
    );
?>

<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");


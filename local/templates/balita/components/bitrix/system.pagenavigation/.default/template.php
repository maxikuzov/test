<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$pageTemplate = '?page=';
$page = (int)$arResult["nStartPage"];
?>

<div class="row">
    <div class="col-md-12 text-center">
        <nav aria-label="Page navigation" class="text-center">
            <ul class="pagination">
                <?php if ($arResult['NavPageNomer'] > 1) { ?>
                    <li class="page-item">
                        <a class="page-link" href="<?= $pageTemplate . (int)$arResult['NavPageNomer'] - 1 ?>">Prev</a>
                    </li>
                <?php }
                    while ($page <= $arResult['nEndPage']) {
                        if ($page === (int)$arResult['NavPageNomer']) { ?>
                            <li class="page-item active">
                                <a class="page-link"><?= $page ?></a>
                            </li>
                        <?php } else { ?>
                            <li class="page-item">
                                <a class="page-link" href="<?= $pageTemplate . $page ?>"><?= $page ?></a>
                            </li>
                        <?php }
                        $page++; ?>
                    <?php } ?>
                <?php if ($arResult['NavPageNomer'] < $arResult['NavPageCount']) { ?>
                    <li class="page-item"><a class="page-link" href="<?= $pageTemplate . (int)$arResult['NavPageNomer'] + 1 ?>">Next</a></li>
                <?php } ?>
            </ul>
        </nav>
    </div>
</div>


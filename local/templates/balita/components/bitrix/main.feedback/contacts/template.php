<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<section class="site-section">
    <div class="container">
        <div class="row mb-4">
            <div class="col-md-6">
                <h1>Contact</h1>
            </div>
        </div>
        <div class="row blog-entries">
            <div class="col-md-12 col-lg-8 main-content">
                <?php if (!empty($error = $arResult["ERROR_MESSAGE"])) { ?>
                    <p><?= $error ?></p>
                <?php }
                      if (!empty($successMessage = $arResult["OK_MESSAGE"])) { ?>
                    <p><?= $successMessage ?></p>
                <?php } ?>
                <form action="/contacts/" method="POST">
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label for="name">Name</label>
                            <input type="text" id="name" class="form-control " name="user_name">
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="phone">Phone</label>
                            <input type="text" id="phone" class="form-control ">
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="email">Email</label>
                            <input type="email" id="email" class="form-control " name="user_email">
                        </div>
                        <div class="col-md-4 form-group">
                            <?= bitrix_sessid_post() ?>
                        </div>
                        <div class="col-md-4 form-group">
                            <input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="message">Write Message</label>
                            <textarea name="MESSAGE" id="message" class="form-control " cols="30" rows="8"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="submit" value="Send Message" class="btn btn-primary" name="submit">
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-12 col-lg-4 sidebar">
                <?php
                $APPLICATION->IncludeComponent("bitrix:main.include", "",
                    [
                        "PATH" => SITE_DIR . "/include/sidebar/search.php",
                        "AREA_FILE_SHOW" => "file",
                    ],
                    false
                );
                $APPLICATION->IncludeComponent("bitrix:main.include", "",
                    [
                        "PATH" => SITE_DIR . "/include/sidebar/bio.php",
                        "AREA_FILE_SHOW" => "file",
                    ],
                    false
                );
                $APPLICATION->IncludeComponent("bitrix:main.include", "",
                    [
                        "PATH" => SITE_DIR . "/include/sidebar/popular_posts.php",
                        "AREA_FILE_SHOW" => "file",
                    ],
                    false
                );
                $APPLICATION->IncludeComponent(
                    'App:section.list',
                    'articles_sections',
                    [
                        'SHOW_COUNT_ELEMENTS' => true
                    ]
                );
                ?>
            </div>
        </div>
    </div>
</section>

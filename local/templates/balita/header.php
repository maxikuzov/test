<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Page\Asset;
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <title><?= $APPLICATION->ShowTitle() ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php
        Asset::getInstance()->addString('<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300, 400,700" rel="stylesheet">');
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/bootstrap.css');
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/animate.css');
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/owl.carousel.min.css');
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/style.css');
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/fonts/ionicons/css/ionicons.min.css');
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/fonts/fontawesome/css/font-awesome.min.css');
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/fonts/flaticon/font/flaticon.css');
    ?>
</head>
    <?php $APPLICATION->ShowPanel(); ?>
<body>
    <header role="banner">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-9 social">
                        <a href="#"><span class="fa fa-twitter"></span></a>
                        <a href="#"><span class="fa fa-facebook"></span></a>
                        <a href="#"><span class="fa fa-instagram"></span></a>
                        <a href="#"><span class="fa fa-youtube-play"></span></a>
                        <a href="#"><span class="fa fa-vimeo"></span></a>
                        <a href="#"><span class="fa fa-snapchat"></span></a>
                    </div>
                    <div class="col-3 search-top">
                        <!-- <a href="#"><span class="fa fa-search"></span></a> -->
                        <form action="#" class="search-top-form">
                            <span class="icon fa fa-search"></span>
                            <input type="text" id="s" placeholder="Type keyword to search...">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="container logo-wrap">
            <div class="row pt-5">
                <div class="col-12 text-center">
                    <a class="absolute-toggle d-block d-md-none" data-toggle="collapse" href="#navbarMenu" role="button" aria-expanded="false" aria-controls="navbarMenu"><span class="burger-lines"></span></a>
                    <h1 class="site-logo">
                        <a href="/">Balita</a></h1>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-md  navbar-light bg-light">
            <div class="container">
                <div class="collapse navbar-collapse" id="navbarMenu">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item">
                            <a class="nav-link active" href="/">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="/articles/" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
                            <?php
                                $APPLICATION->IncludeComponent(
                                    'App:section.list',
                                    'articles_menu',
                                    []
                                );
                            ?>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/about/">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/contacts/">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

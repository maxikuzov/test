<?php

declare(strict_types=1);

namespace App\Services;

use Bitrix\Main\Loader;
use CIBlockSection;

/**
 * Класс-сервис для работы с разделами инфоблока
 */
class SectionService
{
    /**
     * Получает названия секций и ее ссылку по Ids
     *
     * @param int $iblockId - ИД инфоблока
     * @param array $sectionIds - ID секций
     * @return array
     */
    public static function getSectionNamesByIds(int $iblockId, array $sectionIds): array
    {
        Loader::includeModule("iblock");

        $sectionNames = [];

        $sectionIds = array_unique($sectionIds);
        $arFilter = [
            'IBLOCK_ID' => $iblockId,
            'ID' => $sectionIds,
            'ACTIVE' => 'Y',
        ];

        $arSelect = [
            'ID',
            'NAME',
            'SECTION_PAGE_URL'
        ];

        $res = CIBlockSection::GetList(
            [],
            $arFilter,
            false,
            $arSelect,
            false,
        );

        while ($result = $res->GetNext()) {
            $sectionNames[$result['ID']] = $result;
        }

        return $sectionNames;
    }
}


<?php

declare(strict_types=1);

namespace App\Services;

use CFile;

/**
 * Класс-сервис для работы с файлами
 */
class FileService
{
    /**
     * Получает пути к файлам по Id
     *
     * @param array $filesIds - id файлов в таблице
     * @return array
     */
    public static function getFilePath(array $filesIds): array
    {
        $filePath = [];

        $strIds = implode(",", $filesIds);

        $res = CFile::GetList([], [
            "@ID" => $strIds
        ]);

        while ($result = $res->GetNext()) {
            $result["SRC"] = "/upload/" . $result["SUBDIR"] . "/" . $result["FILE_NAME"];
            $filePath[$result["ID"]] = $result['SRC'];
        }

        return $filePath;
    }
}

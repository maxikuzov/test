<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<div class="sidebar-box">
    <h3 class="heading">Categories</h3>
    <ul class="categories">
        <?php foreach ($arResult['ITEMS'] as $item) { ?>
            <li>
                <a href="<?= $item['LINK'] ?>"><?= $item['NAME'] ?>
                    <span><?= !empty($item['COUNT']) ? '(' . $item['COUNT'] . ')' : '' ?></span>
                </a>
            </li>
        <?php } ?>
    </ul>
</div>



<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<div class="dropdown-menu" aria-labelledby="dropdown05">
    <?php foreach ($arResult['ITEMS'] as $item) { ?>
        <a class="dropdown-item" href="<?= $item['LINK'] ?>"><?= $item['NAME'] ?></a>
    <?php } ?>
</div>

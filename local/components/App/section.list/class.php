<?php

declare(strict_types=1);

use Bitrix\Main\Loader;
use App\Services\SectionService;
use CIBlockElement;

/**
 * Класс компонента для вывода разделов
 */
class SectionList extends CBitrixComponent
{
    protected array $data = [];

    protected array $countElements = [];

    protected array $sectionNames = [];

    /**
     * Получение данных
     *
     * @return $this
     */
    private function getData(): SectionList
    {
        $this->data = SectionService::getSectionNamesByIds(IBLOCK_ARTICLES_ID, []);

        if ($this->arParams['SHOW_COUNT_ELEMENTS']) {
            $this->getCountElements();
        }

        return $this;
    }

    /**
     * Получает количество элементов в каждом разделе
     *
     * @return $this
     */
    private function getCountElements(): SectionList
    {
        Loader::includeModule("iblock");

        $iblockId = $this->arParams['IBLOCK_ID'];

        $arFilter = [
            'IBLOCK_ID' => !empty($iblockId) ? $iblockId : IBLOCK_ARTICLES_ID,
        ];

        $arSelect = [
            'ID',
            'IBLOCK_ID',
            'SECTION_ID'
        ];

        $res = CIBlockElement::getList(
            [],
            $arFilter,
            [
                'IBLOCK_SECTION_ID'
            ],
            false,
            $arSelect
        );

        while ($result = $res->Fetch()) {
            $this->countElements[$result['IBLOCK_SECTION_ID']] = $result;
        }

        return $this;
    }

    /**
     * Форматирования данных для вывода
     *
     * @return array
     */
    private function formatData(): array
    {
        $result = [];

        foreach ($this->data as $item) {
            $result[$item['ID']] = [
                'NAME' => $item['NAME'],
                'LINK' => $item['SECTION_PAGE_URL']
            ];

            if (!empty($this->countElements[$item['ID']])) {
                $result[$item['ID']]['COUNT'] = $this->countElements[$item['ID']]['CNT'];
            }
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function executeComponent()
    {
        $this->arResult['ITEMS'] = $this->getData()->formatData();

        $this->IncludeComponentTemplate();
        return $this->arResult;
    }
}

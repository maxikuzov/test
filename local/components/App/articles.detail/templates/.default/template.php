<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<section class="site-section py-lg">
    <div class="container">
        <div class="row blog-entries">
            <div class="col-md-12 col-lg-8 main-content">
                <h1 class="mb-4"><?= $arResult['DATA']['NAME'] ?></h1>
                <div class="post-meta">
                    <span class="category"><?= $arResult['DATA']['SECTION_NAME'] ?></span>
                    <span class="mr-2"><?= $arResult['DATA']['DATE'] ?></span> &bullet;
                    <span class="ml-2"><span class="fa fa-comments"></span><?= $arResult['DATA']['COMMENTS'] ?></span>
                </div>
                <div class="post-content-body">
                    <?= $arResult['DATA']['TEXT'] ?>
                    <div class="row mb-5">
                        <div class="col-md-12 mb-4 element-animate">
                            <img src="<?= $arResult['DATA']['DETAIL_PICTURE'] ?>" alt="<?= $arResult['DATA']['NAME'] ?>" class="img-fluid">
                        </div>
                        <?php foreach ($arResult['DATA']['IMAGES'] as $item) { ?>
                            <div class="col-md-6 mb-4 element-animate">
                                <img src="<?= $item ?>" alt="<?= $arResult['DATA']['NAME'] ?>" class="img-fluid">
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="pt-5">
                    <p>Categories: <a href="#"><?= $arResult['DATA']['SECTION_NAME'] ?></a></p>
                </div>
            </div>
            <div class="col-md-12 col-lg-4 sidebar">
                <?php
                $APPLICATION->IncludeComponent("bitrix:main.include", "",
                    [
                        "PATH" => SITE_DIR . "/include/sidebar/search.php",
                        "AREA_FILE_SHOW" => "file",
                    ],
                    false
                );
                $APPLICATION->IncludeComponent("bitrix:main.include", "",
                    [
                        "PATH" => SITE_DIR . "/include/sidebar/bio.php",
                        "AREA_FILE_SHOW" => "file",
                    ],
                    false
                );
                $APPLICATION->IncludeComponent("bitrix:main.include", "",
                    [
                        "PATH" => SITE_DIR . "/include/sidebar/popular_posts.php",
                        "AREA_FILE_SHOW" => "file",
                    ],
                    false
                );
                $APPLICATION->IncludeComponent(
                    'App:section.list',
                    'articles_sections',
                    [
                        'SHOW_COUNT_ELEMENTS' => true
                    ]
                );
                ?>
            </div>
        </div>
    </div>
</section>


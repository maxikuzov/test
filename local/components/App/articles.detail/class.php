<?php

declare(strict_types=1);

use Bitrix\Main\Loader;
use App\Services\FileService;
use CIBlockElement;

class ArticlesDetail extends CBitrixComponent
{
    protected array $data = [];

    protected array $imageSrc = [];

    protected string $sectionName = '';

    protected string $sectionCode = '';

    protected string $articleCode = '';

    /**
     * @inheritDoc
     */
    public function onPrepareComponentParams($params)
    {
        $params = parent::onPrepareComponentParams($params);

        $sectionCode = $this->request->getQuery('section_code');
        $articleCode = $this->request->getQuery('article_code');

        $this->sectionCode = !empty($params['SECTION_CODE'])
            ? $params['SECTION_CODE']
            : $sectionCode;

        $this->articleCode = !empty($params['ARTICLE_CODE'])
            ? $params['ARTICLE_CODE']
            : $articleCode;

        return $params;
    }

    /**
     * Получает название раздела статьи
     *
     * @return string
     */
    private function getSectionName(string $sectionId): string
    {
        $sectionName = '';

        if (!empty($sectionId)) {
            $sectionName = CIBlockSection::GetByID($sectionId)->GetNext();
        }

        return $sectionName['NAME'];
    }

    /**
     * Получение данных об элементе
     *
     * @return $this
     */
    private function getData(): ArticlesDetail
    {
        Loader::includeModule("iblock");

        $iblockId = $this->arParams['IBLOCK_ID'];

        $arFilter = [
            'IBLOCK_ID' => !empty($iblockId) ? $iblockId : IBLOCK_ARTICLES_ID,
            'SECTION_CODE' => $this->sectionCode,
            'CODE' => $this->articleCode
        ];

        $arSelect = [
            'ID',
            'IBLOCK_ID',
            'IBLOCK_SECTION_ID',
            'NAME',
            'DATE_CREATE',
            'DETAIL_TEXT',
            'DETAIL_PICTURE'
        ];

        $res = CIBlockElement::getList(
            [],
            $arFilter,
            false,
            false,
            $arSelect
        )->GetNextElement();

        $result = array_merge($res->getFields(), $res->getProperties());

        $this->data = $result;

        $imageIds[] = $result['DETAIL_PICTURE'];
        $imageIds = !empty($result['IMAGES']['VALUE'])
            ? array_merge($imageIds, $result['IMAGES']['VALUE']) : [];
        $this->sectionName = $this->getSectionName($this->data['IBLOCK_SECTION_ID']);
        $this->imageSrc = FileService::getFilePath($imageIds);

        return $this;
    }

    /**
     * Форматирования данных для вывода
     *
     * @return array
     */
    private function formatData(): array
    {
        $images = !empty($this->data['IMAGES']['VALUE']) ? $this->data['IMAGES']['VALUE'] : [];

        $result = [
            'NAME' => $this->data['NAME'],
            'SECTION_NAME' => $this->sectionName,
            'DATE' => date('F d, Y', MakeTimeStamp($this->data['DATE_CREATE'])),
            'COMMENTS' => $this->data['AMOUNT_COMMENTS']['VALUE'],
            'TEXT' => $this->data['DETAIL_TEXT'],
            'DETAIL_PICTURE' => $this->imageSrc[$this->data['DETAIL_PICTURE']],
            'IMAGES' => array_map(function ($val) {
                return $this->imageSrc[$val];
            }, $images)
        ];

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function executeComponent()
    {
        $this->arResult['DATA'] = $this->getData()->formatData();

        $this->IncludeComponentTemplate();
        return $this->arResult;
    }
}

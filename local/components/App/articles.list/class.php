<?php

declare(strict_types=1);

use Bitrix\Main\Loader;
use App\Services\SectionService;
use App\Services\FileService;
use CIBlockElement;

/**
 * Класс компонент для вывода статей
 */
class ArticlesList extends CBitrixComponent
{
    protected array $data = [];

    protected string $sectionCode = '';

    protected array $imageSrc = [];

    protected array $sectionNames = [];

    protected string $pagenation = '';

    protected const MAX_ELEMENTS_PAGE = 8;

    /**
     * @inheritDoc
     */
    public function onPrepareComponentParams($params)
    {
        $params = parent::onPrepareComponentParams($params);

        if (!empty($code = $params['SECTION_CODE'])) {
            $this->sectionCode = $code;
        } else if ($code = $this->request->getQuery('section')) {
            $this->sectionCode = $code;
        }

        return $params;
    }

    /**
     * Получает параметры пагинации
     *
     * @return array
     */
    private function getPagenParams(): array
    {
        if (!empty($page = $this->request->getQuery('page'))) {
            $pageParam = $page;
        }

        $maxElements = $this->arParams['MAX_ELEMENTS_PAGE'];

        return [
            "nTopCount" => false,
            "bShowAll" => false,
            "nPageSize" => !empty($maxElements) ? $maxElements : self::MAX_ELEMENTS_PAGE,
            "iNumPage" => !empty($pageParam) ? $pageParam : 1,
        ];
    }

    /**
     * Получает название текущего раздела
     *
     * @return string
     */
    private function getSectionName(): string
    {
        $sectionName = '';

        if (!empty($this->arParams['SECTION_CODE']) || !empty($this->request->getQuery('section'))) {
            $sectionName = array_shift($this->sectionNames)['NAME'];
        }

        return $sectionName;
    }

    /**
     * Получение данных
     *
     * @return AriclesList
     */
    private function getData(): ArticlesList
    {
        Loader::includeModule("iblock");

        $iblockId = $this->arParams['IBLOCK_ID'];

        $defaultFilter = [
            'IBLOCK_ID' => !empty($iblockId) ? $iblockId : IBLOCK_ARTICLES_ID,
            'ACTIVE' => 'Y'
        ];

        $arFilter = $defaultFilter;
        if (!empty($this->sectionCode)) {
            $arFilter['SECTION_CODE'] = $this->sectionCode;
        }

        $arOrder = [
            'SORT' => 'ASC'
        ];

        $pagen = $this->getPagenParams();

        $arSelect = [
            'ID',
            'IBLOCK_ID',
            'IBLOCK_SECTION_ID',
            'DATE_CREATE',
            'NAME',
            'DETAIL_PAGE_URL',
            'PREVIEW_PICTURE',
        ];

        $res = CIBlockElement::getList(
            $arOrder,
            $arFilter,
            false,
            $pagen,
            $arSelect
        );

        $sectionIds = [];
        $imagesIds = [];
        $this->pagenation = $res->GetPageNavStringEx($navComponentObject, "", '.default');

        while ($result = $res->GetNextElement()) {
            $fields = $result->getFields();
            $props = $result->getProperties();

            $sectionIds[] = $fields['IBLOCK_SECTION_ID'];
            $imagesIds[] = $fields['PREVIEW_PICTURE'];

            $this->data[$fields['ID']] = array_merge($fields, $props);
        }

        $this->sectionNames = SectionService::getSectionNamesByIds(IBLOCK_ARTICLES_ID, $sectionIds);
        $this->imageSrc = FileService::getFilePath($imagesIds);

        return $this;
    }

    /**
     * Форматирования данных для вывода
     *
     * @return array
     */
    private function formatData(): array
    {
        $result = [];

        foreach ($this->data as $item) {
            $result[$item['ID']] = [
                'NAME' => $item['NAME'],
                'DATE' => date('F d, Y', MakeTimeStamp($item['DATE_CREATE'])),
                'SECTION_NAME' => $this->sectionNames[$item['IBLOCK_SECTION_ID']]['NAME'],
                'PREVIEW_PICTURE' => $this->imageSrc[$item['PREVIEW_PICTURE']],
                'COMMENTS' => $item['AMOUNT_COMMENTS']['VALUE'],
                'DETAIL_PAGE' => $item['DETAIL_PAGE_URL']
            ];
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function executeComponent()
    {
        // todo: здесь можно все закешировать, привязав выборку к тегированному кешу инфоблока
        // todo: обычно для этого использую или пишу какие-нибудь сервисы или обертки битриксового кеша с методами из D7
        $this->arResult['ITEMS'] = $this->getData()->formatData();
        $this->arResult['SECTION_NAME'] = $this->getSectionName();
        $this->arResult['NAV'] = $this->pagenation;

        $this->IncludeComponentTemplate();
        return $this->arResult;
    }
}

<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<section class="site-section">
    <div class="container">
        <div class="row mb-4">
            <div class="col-md-6">
                <h2 class="mb-4">Category: <?= $arResult['SECTION_NAME'] ?></h2>
            </div>
        </div>
        <div class="row blog-entries">
            <div class="col-md-12 col-lg-8 main-content">
                <div class="row mb-5 mt-5">
                    <div class="col-md-12">
                    <?php foreach ($arResult['ITEMS'] as $item) { ?>
                        <div class="post-entry-horzontal">
                            <a href="<?= $item['DETAIL_PAGE'] ?>">
                                <div class="image element-animate" data-animate-effect="fadeIn" style="background-image: url(<?= $item['PREVIEW_PICTURE'] ?>);"></div>
                                <span class="text">
                                <div class="post-meta">
                                    <span class="category"><?= $item['SECTION_NAME'] ?></span>
                                    <span class="mr-2"><?= $item['DATE'] ?></span> &bullet;
                                    <span class="ml-2"><span class="fa fa-comments"></span> 3</span>
                                </div>
                                <h2><?= $item['NAME'] ?></h2>
                                </span>
                            </a>
                        </div>
                    <?php } ?>
                    </div>
                </div>
                <?= $arResult['NAV'] ?>
            </div>
            <div class="col-md-12 col-lg-4 sidebar">
                <?php
                    $APPLICATION->IncludeComponent("bitrix:main.include", "",
                        [
                            "PATH" => SITE_DIR . "/include/sidebar/search.php",
                            "AREA_FILE_SHOW" => "file",
                        ],
                        false
                    );
                    $APPLICATION->IncludeComponent("bitrix:main.include", "",
                        [
                            "PATH" => SITE_DIR . "/include/sidebar/bio.php",
                            "AREA_FILE_SHOW" => "file",
                        ],
                        false
                    );
                    $APPLICATION->IncludeComponent("bitrix:main.include", "",
                        [
                            "PATH" => SITE_DIR . "/include/sidebar/popular_posts.php",
                            "AREA_FILE_SHOW" => "file",
                        ],
                        false
                    );
                    $APPLICATION->IncludeComponent(
                        'App:section.list',
                        'articles_sections',
                        [
                            'SHOW_COUNT_ELEMENTS' => true
                        ]
                    );
                ?>
            </div>
        </div>
    </div>
</section>

<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<?php if ($arResult['ITEMS']) { ?>
    <div class="col-md-12 col-lg-8 main-content">
        <div class="row">
            <?php foreach ($arResult['ITEMS'] as $item) { ?>
                <div class="col-md-6">
                    <a href="<?= $item['DETAIL_PAGE'] ?>" class="blog-entry element-animate" data-animate-effect="fadeIn">
                        <img src="<?= $item['PREVIEW_PICTURE'] ?>" alt="<?= $item['NAME'] ?>">
                        <div class="blog-content-body">
                            <div class="post-meta">
                                <span class="category"><?= $item['SECTION_NAME'] ?></span>
                                <span class="mr-2"><?= $item['DATE'] ?></span> &bullet;
                                <span class="ml-2"><span class="fa fa-comments"></span><?= $item['COMMENTS'] ?></span>
                            </div>
                            <h2><?= $item['NAME'] ?></h2>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
        <?= $arResult['NAV'] ?>
    </div>
<?php } ?>

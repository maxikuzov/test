<?php

$arUrlRewrite = [
  0 => [
      'CONDITION' => '#^/rest/#',
      'RULE' => '',
      'ID' => NULL,
      'PATH' => '/bitrix/services/rest/index.php',
      'SORT' => 100,
  ],
  1 => [
      'CONDITION' => '#^/articles/(.+?)/(.+?)#',
      'RULE' => 'section_code=$1&article_code=$2',
      'ID' => NULL,
      'PATH' => '/articles/detail.php',
      'SORT' => 100,
  ],
  2 => [
      'CONDITION' => '#^/articles/(.+?)#',
      'RULE' => 'section=$1',
      'ID' => NULL,
      'PATH' => '/articles/section.php',
      'SORT' => 100,
  ],
];

<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("Тестовое задание");

$APPLICATION->IncludeComponent("bitrix:main.include", "",
    [
        "PATH" => SITE_DIR . "/include/sidebar/banner.php",
        "AREA_FILE_SHOW" => "file",
    ],
    false
);

// todo: я бы не выводил сюда section, но внутри него как раз верстка статей, либо можно было бы сделать комплексный компонент blog
?>

<section class="site-section py-sm">
    <div class="container">
        <?php
            $APPLICATION->IncludeComponent("bitrix:main.include", "",
                [
                    "PATH" => SITE_DIR . "/include/sidebar/title_main_page.php",
                    "AREA_FILE_SHOW" => "file",
                ],
                false
            );
        ?>
        <div class="row blog-entries">
            <?php
                $APPLICATION->IncludeComponent(
                    'App:articles.list',
                    '.default'
                );
            ?>
            <div class="col-md-12 col-lg-4 sidebar">
                <?php
                    $APPLICATION->IncludeComponent("bitrix:main.include", "",
                        [
                            "PATH" => SITE_DIR . "/include/sidebar/search.php",
                            "AREA_FILE_SHOW" => "file",
                        ],
                        false
                    );
                    $APPLICATION->IncludeComponent("bitrix:main.include", "",
                        [
                            "PATH" => SITE_DIR . "/include/sidebar/bio.php",
                            "AREA_FILE_SHOW" => "file",
                        ],
                        false
                    );
                    $APPLICATION->IncludeComponent("bitrix:main.include", "",
                        [
                            "PATH" => SITE_DIR . "/include/sidebar/popular_posts.php",
                            "AREA_FILE_SHOW" => "file",
                        ],
                        false
                    );
                    $APPLICATION->IncludeComponent(
                        'App:section.list',
                        'articles_sections',
                        [
                            'SHOW_COUNT_ELEMENTS' => true
                        ]
                    );
                ?>
            </div>
        </div>
    </div>
</section>

<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");

<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("Тестовое задание");
?>

<?php
    $APPLICATION->IncludeComponent(
        'App:articles.list',
        'section'
    );
?>

<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
